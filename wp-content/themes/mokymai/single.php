<?php get_header(); ?>

<?php if ( have_posts() ) : 
	while ( have_posts() ) : the_post(); ?>

    <main role="main">
      <?php 
        the_post_thumbnail('medium');
      ?>
      <section class="jumbotron text-center">
        <div class="container">
          <div class="row">
            <div class="col-md-8">
              <h1 class="jumbotron-heading"><?php the_title(); ?></h1>
                  <p class="lead text-muted"><?php the_date(); ?></p>
                   <?php 
                       $autorius =  get_the_author_meta('display_name');
                        echo $autorius; 

                      ?>
                 
                </div>
            <div class="col-md-4">
              
                <?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
                  <div>
                    <?php dynamic_sidebar( 'home_right_1' ); ?>
                  </div>
                <?php endif; ?>
              </div>
          
      </section>

      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
          	<p>

          		<?php the_content(); ?>
          	</p>
          	<div class="comments">
          		<?php 
          			// If comments are open or we have at least one comment, load up the comment template.
 					if ( comments_open() || get_comments_number() ) :
    					comments_template();
 					endif;
          		?>
          	</div>
           </div>
        </div>
    </div>

    </main>
<?php  endwhile; 
			endif; 
?>
<?php get_footer(); ?>