<?php 
/**
 * Template Name: Kontaktu puslapis
 *
 */

get_header(); ?>

<?php if ( have_posts() ) : 
	while ( have_posts() ) : the_post(); ?>

    <main role="main">
      Filialai:
      <?php 
        $filialai = get_pages(['child_of' => get_the_ID()]); ?>

        <select>
        <?php 
        foreach($filialai as $filialas) {
          echo "<option>";
          echo $filialas->post_title;
          echo "</option>";
        }
      ?>
       </select>
      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading"><?php the_title(); ?></h1>
          <p class="lead text-muted"></p>
         
        </div>
      </section>
      <div class="mapouter">
      	<div class="gmap_canvas">
      	<iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=kaunas&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.crocothemes.net"></a></div><style>.mapouter{text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div>

      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
          	<p>
          		<?php the_content(); ?>
          	</p>
           </div>
        </div>
    </div>

    </main>
<?php  endwhile; 
			endif; 
?>
<?php get_footer(); ?>