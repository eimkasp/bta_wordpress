<?php get_header(); ?>
  
    
    <main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading"><?php echo bloginfo('name'); ?></h1>
          <p class="lead text-muted"><?php bloginfo('description'); ?></p>
          <p>
            <a href="#" class="btn btn-primary my-2">Main call to action</a>
            <a href="#" class="btn btn-secondary my-2">Secondary action</a>
          </p>
        </div>
      </section>

      <div class="album py-5 bg-light">
      	<div class="row">
        <div class="container">
        	<?php 
        		$args = array(
					'post_type' => 'products',
					'post_status' => 'publish',
					'posts_per_page' => -1, // visas naujienas kiek radome atvaizduoti
				);

				$query = new WP_Query( $args );

				while ( $query->have_posts() ) :
					$query->the_post(); ?>
					<div class="col-md-4">
		              <div class="card mb-4 shadow-sm">
		                <div class="card-body">
		                  <p class="card-text">
		                  	<a href="<?php the_permalink(); ?>">
		                  		<?php the_title(); ?>
		                  	</a>
		                  	</p>
		                  <div class="d-flex justify-content-between align-items-center">
		                    <div class="btn-group">
		                      <a href="<?php the_permalink(); ?>" type="button" class="btn btn-sm btn-outline-secondary">
		                      View
		                  </a>
		                      <button type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
		                    </div>
		                    <small class="text-muted"><?php the_field('kaina'); ?></small>
		                  </div>
		                </div>
		              </div>
		            </div>

					
				<?php endwhile;
        	?>
         </div>
     </div>
 </div>

    </main>

<?php get_footer(); ?>