<?php get_header(); ?>

<?php if ( have_posts() ) : 
	while ( have_posts() ) : the_post(); ?>

    <main role="main">

      <section class="jumbotron text-center">
        <div class="container">
          <h1 class="jumbotron-heading"><?php the_title(); ?></h1>
          <p class="lead text-muted">
          	<?php the_field('kaina'); ?>
          </p>
         
        </div>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
          	<p>
          		<?php the_content(); ?>
          	</p>
           </div>
           <?php 
                // If comments are open or we have at least one comment, load up the comment template.
          if ( comments_open() || get_comments_number() ) :
              comments_template();
          endif;
              ?>
        </div>
    </div>

    </main>
<?php  endwhile; 
			endif; 
?>
<?php get_footer(); ?>