<?php

register_post_type( 'products',
    array(
      'labels' => array(
        'name' => __( 'Products' ),
        'singular_name' => __( 'Product' )
      ),
      'public' => true,
      'supports' => array( 'title', 'editor', 'comments', 'featured-image'),

      'has_archive' => true,
    )
);

register_nav_menus(
			array(
					'top-menu' => __( 'Header Menu' ),
					'footer-menu' => __( 'Footer Menu' )
			)
);

// Prideti role darbuotojas
add_role(
    'darbuotojas',
    __( 'Darbuotojas' ),
    array(
    'read'         => true, 
    'edit_posts'   => true,
    'delete_posts' => false
    ));

// pagrinidines nuotraukos ijungimas
add_theme_support( 'post-thumbnails' );


add_image_size( 'post-main', 900, 9999 );




function widgets_reg_init() {

  register_sidebar( array(
    'name'          => 'Home right sidebar',
    'id'            => 'home_right_1',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="rounded">',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => 'Footer sidebar',
    'id'            => 'footer',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="rounded">',
    'after_title'   => '</h2>',
  ) );

}
add_action( 'widgets_init', 'widgets_reg_init' );

