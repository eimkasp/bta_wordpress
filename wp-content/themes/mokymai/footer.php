    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <?php wp_nav_menu( array( 'theme_location' => 'top-menu' ) ); ?>
      </div>
    </footer>   
    <?php wp_footer(); ?>
  </body>
</html>