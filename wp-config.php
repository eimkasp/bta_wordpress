<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bta_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A,J01%g1UEYE`Dr46_KV,:|}gDCaIqLeG,NS^Rq.7r31#Q!Srk|$~c$~Z4OzSANk');
define('SECURE_AUTH_KEY',  'SOgIjs+*F-)LP^E=`_#`z`Sv7sj%-9a;w5 AV(<&W++hpuqa0bmG_|gR?<Ax`a}F');
define('LOGGED_IN_KEY',    '4ipI}!vqH}_ZafY6 s&@FI!T5LqeT@F5a?Ov6 ex4eaOtXbP@MLn=9`q=@ngp6+~');
define('NONCE_KEY',        '5zeu@E]{+h4)^0!OQ)DRv#R1]#`^2eW)J]!4^HC!410cI]Y:l,hI<kv&NaVlh^g{');
define('AUTH_SALT',        'P6E$!LZZqajbw)I?vao%$Rip:!y]dE43b/Jtixc[~KR{r}i-7,xexv rPeT<&Ra&');
define('SECURE_AUTH_SALT', 'fmj@6G<gSuk5I&~8TJ|HwhDdU$G/D.*h`y*DUuf;N!lUV*=LAc5co%dv,!ngv6Su');
define('LOGGED_IN_SALT',   '[[<T<2S!e{gLA8!N~R>7!2w6@nopyj@yrY~z3HN~-$<Z?BGY;mnxga3.j]7^84N$');
define('NONCE_SALT',       'uQ  `j +u|$:V]G,JE%[Z:$V%*O,b<c9|kSFfFy2;f$tFwopfXYLA._n=AP+^#KZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
